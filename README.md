# WebSiteKitについて #

## 概要 ##

WebSiteKitはソニックムーブにおけるWebサイト実装のためのものである。


## 使い方 ##

以下を順番に実行し、各プロジェクト毎のフォーマットデータを作成する。  
※{{}}は自己都合に合わせたものに置換する。

```
$ cd {{ローカル作業フォルダのパス}}
$ git clone git@bitbucket.org:sonicmoov/markup-websitekit.git {{使用するプロジェクト名}}
$ cd {{使用するプロジェクト名}}
$ rm -rf .git
```

実際に使用する際はプロジェクト毎に以下を書き換えて使用する事とし、ここまでの内容は消去する事。

---

# {{ プロジェクト名 }} #

## 概要 ##

{{ 案件の概要を記述する }}

## 案件フォルダ ##

smb://172.17.17.17/smv/{{XXXX}}

## 使い方 ##

### データダウンロード ###
```
$ cd {{ローカルのパス}}
$ git clone git@bitbucket.org:{{xxx}}/{{yyy}}.git
$ cd {{yyy}}
```

### node moduleインストール ###
```
$ npm install
```

### gulp実行 ###

#### ローカルでの開発データコンパイル ####
```
$ gulp
```

#### ローカル外の確認データ・納品データコンパイル ####
```
$ gulp dist
```

#### バリデート実行 ####

```
// HTMLのバリデート
$ gulp validateHtml

// Javascriptのバリデート
$ gulp validateJs
```


## 検証環境 ##

### PC ###

### Windows ###
- IE 11
- Edge最新版
- chrome最新版

### Mac ###
- safari最新版  

### SP ###

### iOS ###
- iOS10〜

### Android ###
- Android 4.4〜